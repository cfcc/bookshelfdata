<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 8/29/16
 * Time: 10:53 AM
 */
try {
    session_start();
    $_SESSION = array();
    session_destroy();

    header('HTTP/1.1 200 Authentication: Logged Out');
} catch (Exception $e) {
    header('HTTP/1.1 500 ' . "Caught Exception:" . $e->getMessage());
}
