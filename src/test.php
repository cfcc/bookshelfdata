<!DOCTYPE html>
<html lang="en">
<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 10/3/16
 * Time: 2:48 PM
 */

require_once("include/app_config.php");
require_once("include/database.php");
require_once("include/functions.php");

$title = "Bookshelf Data";
$page_title = "Test";

session_start();

?>
<head>
    <?php require_once "include/header.php"; ?>
</head>
<body>
<?php require_once "include/navbar.php"; ?>
<div class="container">
    <?php
    if (isset($_SESSION['user_info'])) {
        ?>
        <div class="starter-template-alt">
            <div class="page-header"><h1><?= $page_title ?></h1></div>
            <div class="row">
                <?php
                $result = getBooksByCourse2($dsn, $config['db']['username'], $config['db']['password']);
                echo "<pre>\n";
                print_r($result);
                echo "</pre>\n";
                echo "<hr>\n";
                $exclusive = findExclusive($result);
                echo "<pre>\n";
                print_r($exclusive);
                echo "</pre>\n";
                ?>
            </div>
        </div>
        <?php
    } else {
        echo "<div class='starter-template'>\n";
        echo "<img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>\n";
        echo "</div>\n";
    }
    ?>
</div>
<?php require_once("include/footer.php"); ?>
</body>
</html>
