<!DOCTYPE html>
<html lang="en">
<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 6/30/16
 * Time: 2:39 PM
 */

require_once("include/app_config.php");
require_once("include/database.php");

$title = "Bookshelf Data";
$page_title = "Import";

session_start();

?>
<head>
    <?php require_once "include/header.php"; ?>
</head>
<body>
<?php require_once "include/navbar.php"; ?>
<div class="container">
    <?php
    if (isset($_SESSION['user_info'])) {
        ?>
        <div class="starter-template-alt">
            <div class="page-header"><h1><?= $page_title ?></h1></div>
            <div class="row">
                <form enctype="multipart/form-data" action="import.php" method="POST" class="form-horizontal">
                    <div class="form-group">
                        <label for="csvfile" class="col-sm-2 control-label">CSV File</label>
                        <div class="col-sm-10">
                            <input type="file" id="csvfile" name="csvfile"/>
                            <p class="help-block">Select a file from your computer.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" value="Upload"/>
                        </div>
                    </div>
                </form>
                <?php
                if (!empty($_FILES['csvfile'])) {
                    if ($_FILES['csvfile']['size'] > 0) {
                        $config = load_config();

                        print("<p>Upload Results</p>\n<pre>");
                        $csv_fn = $_FILES['csvfile']['tmp_name'];

                        $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['dbname'] . ';charset=' . $config['db']['charset'];

                        print("Connecting to: $dsn\n\n");

                        loadColsFromFile($csv_fn, $dsn, $config['db']['username'], $config['db']['password']);

                        print("done</pre>\n");
                    } else {
                        print("<p class='lead text-warning'>Unable to import an empty file.</p>\n");
                    }
                }
                ?>
            </div>
        </div>
        <?php
    } else {
        echo "<div class='starter-template'>\n";
        echo "<img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>\n";
        echo "</div>\n";
    }
    require_once "include/footer.php"; ?>
</div>
</body>
</html>
