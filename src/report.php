<!DOCTYPE html>
<html lang="en">
<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 6/29/16
 * Time: 2:52 PM
 */

$debug = false;

const MAX_SECTIONS_PER_ROW = 15;
const MERGE_EXCLUSIVE = true;

require_once("include/app_config.php");
require_once("include/database.php");
require_once("include/functions.php");

require_once("vendor/cfcc/messages-module/MessagesModuleClass.php");

$title = "Bookshelf Data";
$page_title = "Report";

session_start();

if (array_key_exists('dept', $_REQUEST)) {
    $dept_filter = $_REQUEST['dept'];
} else {
    $dept_filter = '';
}
if ($dept_filter == '') {
    $dept_placeholder = 'All';
} else {
    $dept_placeholder = $dept_filter;
}

if (array_key_exists('course', $_REQUEST)) {
    $course_filter = $_REQUEST['course'];
} else {
    $course_filter = '';
}
if ($course_filter == '') {
    $course_placeholder = 'All';
} else {
    $course_placeholder = $course_filter;
}

?>
<head>
    <?php require_once "include/header.php"; ?>
</head>
<body>
    <?php require_once "include/navbar.php"; ?>
    <div class="container">
        <?php

        $config = load_config();

        if (isset($_SESSION['user_info'])) {
            ?>
            <form action="report.php" class="form-inline" id="report-filter" method="get">
                <div class="form-group">
                    <label for="dept" class="control-label">Department</label>
                    <input type="text" id="dept" name="dept" placeholder="<?= $dept_placeholder ?>">
                </div>
                <div class="form-group">
                    <label for="course" class="control-label">Course Number</label>
                    <input type="text" id="course" name="course" placeholder="<?= $course_placeholder ?>">
                </div>
                <input class='btn btn-primary' type="submit" value="Filter">
                <input class='btn btn-default' type="reset" value="Clear">
            </form>
            <?php
            $result = getBooksByCourse2($dsn, $config['db']['username'], $config['db']['password'], $dept_filter, $course_filter);
            if (count($result) == 0) {
                echo "<div class='starter-template'>\n";
                echo "<div class='col-lg-offset-3 col-lg-6'>";
                echo "<div class='alert alert-info' role='alert'>You must import the CSV file before running the shelf cards.</div>\n";
                echo "</div>\n";
                echo "</div>\n";
            }

            if (MERGE_EXCLUSIVE) {
                $exclusive = findExclusive($result);
            } else {
                $exclusive = array('dept' => array(), 'course' => array());
            }

            $obj = new MessagesModuleUtil(DEFAULT_INI_LOCATION);
            try {
                $messages = $obj->getAllMessages();
            } catch (Exception $e) {
                $messages = array();
                echo $e->getMessage();
            }

            $record_cnt = 0;
            foreach ($result as $key => $record) {
                # When the no_text field is blank, the ISBN is 0, and so we'll skip those cards
                if ($key != '0') {
                    $term = getTerm($record);
                    $course_info_block = "<div class='card-block'>\n<div class='row'>\n";
                    $course_info_block .= "<div class='col-lg-8 text-left'><h4><small>" . strip_tags($messages['STORE_TAG']) . "</small></h4></div>\n";
                    $course_info_block .= "<div class='col-lg-4 text-right'><h4><small>" . $term . "</small></h4></div>\n";
                    $course_info_block .= "</div>\n";
                    $course_info_block .= "<div class='row'>\n<div class='col-lg-12'>";
                    $second_mini = false;
                    if (array_key_exists($key, $exclusive['dept'])) {
                        $course_info_block .= "<h2>All ";
                        if (count($exclusive['dept'][$key]) <= 2) {
                            $course_info_block .= implode(" and ", $exclusive['dept'][$key]);
                        } else {
                            $course_info_block .= implode(", ", array_slice($exclusive['dept'][$key], 0, -1));
                            $course_info_block .= ", and " . $exclusive['dept'][$key][-1];
                        }
                        $course_info_block .= " Sections</h2>\n";
                    } else {
                        // Here we're applying an arbitrary limit on the size of the Course text based on the number of courses
                        // that share this card.
                        $crs_cnt = count($record['courses']);
                        if ($crs_cnt <= 2) {
                            $tag_size = 2;
                        } elseif ($crs_cnt <= 4) {
                            $tag_size = 3;
                        } elseif ($crs_cnt <= 6) {
                            $tag_size = 4;
                        } elseif ($crs_cnt <= 8) {
                            $tag_size = 5;
                        } else {
                            $tag_size = 6;
                        }
                        foreach ($record['courses'] as $course_id => $sections) {
                            // We're also going to limit the size of the Courses text based on the number of sections
                            // associated with this set of books, we can't go smaller than H6 though.
                            $sect_cnt_modifier = floor(count($sections) / MAX_SECTIONS_PER_ROW);
                            $tag_size += $sect_cnt_modifier;
                            if ($tag_size > 6) {
                                $tag = "h6";
                            } else {
                                $tag = "h" . $tag_size;
                            }
                            $course_info_block .= "<" . $tag . ">" . $course_id . "- <small>";
                            $first = true;
                            foreach ($sections as $section_number) {
                                if (!$first) {
                                    $course_info_block .= ", ";
                                } else {
                                    $first = false;
                                }
                                $course_info_block .= $section_number;
                                if (endswith($section_number, "Z")) {
                                    $second_mini = true;
                                    $course_info_block .= "*";
                                }
                            }
                            $course_info_block .= "</small></" . $tag . ">";
                        }
                    }
                    $course_info_block .= "</div>\n</div>\n";
                    if ($second_mini) {
                        $course_info_block .= "<div class='row'><div class='col-lg-offset-1 col-lg-10'>\n";
                        $course_info_block .= "<p class='text-center text-muted'>* Second Mini Section</p>\n";
                        $course_info_block .= "</div></div>\n";
                    }

                    $course_info_block .= "<div class='row'><div class='col-lg-12 text-center'>\n";
                    $course_info_block .= "<p class='text-muted'>Please check your Section # carefully. Confirm materials with instructor <strong>before opening</strong></p>\n";
                    $course_info_block .= "</div>\n</div>\n";

                    $course_info_block .= "</div>\n";

                    $course_info_block_footer = "<div class='row'>\n<div class='col-lg-12 text-center'>\n<h5>";
                    if ($record['required'] > 0) {
                        $course_info_block_footer .= "<strong>Requires (" . $record['required'] . ") item";
                        if ($record['required'] > 1) {
                            $course_info_block_footer .= "s";
                        }
                        $course_info_block_footer .= "</strong>";
                        if ($record['choices'] > 0) {
                            $course_info_block_footer .= ": Pick " . $record['required'] . " of " . $record['choices'] . " Text Choices";
                        }
                    } else {
                        $course_info_block_footer .= "All items optional";
                    }
                    $course_info_block_footer .= "</h5>\n</div>\n</div>\n";

                    $card_block_footer = "<div class='row'><div class='col-lg-12 text-center'>PRICES ARE SUBJECT TO CHANGE</div></div>\n";

                    $block_footer = "</div> <!-- card block -->\n</td>\n</tr>\n<tr>\n<td class='course-block'>$course_info_block_footer</td><td class='book-block'>$card_block_footer</td>\n</tr>\n</table>\n";

                    $block_header = "<table class='cfcc-card'><tr><td class='course-block'>\n";
                    $block_header .= $course_info_block;
                    $block_header .= "</td><td class='book-block'>\n<div class='card-block'>\n";

                    $cnt = 0;

                    echo $block_header;

                    $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

                    $total_books = count($record['books']);
                    foreach ($record['books'] as $row) {
                        $cnt++;
                        echo "<div class='book-info'>\n";
                        echo "<div class='row'>\n";
                        if ($row['required_code'] == 'C') {
                            echo "<div class='col-lg-3'><span class='cfcc-choice'>***CHOICE***</span></div>";
                        } elseif ($row['required_code'] == 'O') {
                            echo "<div class='col-lg-3'><span class='cfcc-optional'>***OPTIONAL***</span></div>";
                        } else {
                            // required code == 'R'
                            echo "<div class='col-lg-3'><span class='cfcc-required'>***REQUIRED***</span></div>";
                        }
                        // echo "</div>\n<div class='row'>\n"; // Add this back if you want the choice flag on a separate line
                        echo "<div class='col-lg-3'>" . $row['publisher'] . "</div>\n";
                        echo "<div class='col-lg-3'>" . $row['author'] . "</div>\n";
                        echo "<div class='col-lg-3'>" . $row['isbn'] . "</div>\n";
                        echo "</div>\n<div class='row'>\n";
                        echo "<div class='col-lg-12'>" . mb_strimwidth($row['title'], 0, 64, "...");
                        if ($row['edition']) {
                            echo " - " . $row['edition'] . " edition";
                        }
                        echo "</div>\n";
                        echo "</div>\n<div class='row'>\n";
                        if ($row['course_print_note'] != 'N') {
                            echo "<div class='col-lg-12 text-center'><em>" . $row['course_note'] . "</em></div>";
                        }
                        echo "</div>\n<div class='row'>\n";
                        echo "<div class='col-lg-6'>";
                        if ($row['list_price_new'] > 0.0) {
                            echo $fmt->formatCurrency($row['list_price_new'], "USD") . " (NEW)";
                        }
                        echo "</div>\n"; // col-lg-6
                        echo "<div class='col-lg-6'>";
                        if ($row['list_price_used'] > 0.0) {
                            echo $fmt->formatCurrency($row['list_price_used'], "USD") . " (USED)";
                        }
                        echo "</div>\n"; // col-lg-6
                        echo "</div>\n"; // row
                        echo "<div class='row'>";
                        echo "<div class='col-lg-6'>";
                        if ($row['rental_fee_new'] > 0.0) {
                            echo $fmt->formatCurrency($row['rental_fee_new'], "USD") . " (NEW RENTAL)";
                        }
                        echo "</div>"; // col-lg-6
                        echo "<div class='col-lg-6'>";
                        if ($row['rental_fee_used'] > 0.0) {
                            echo $fmt->formatCurrency($row['rental_fee_used'], "USD") . " (USED RENTAL)";
                        }
                        echo "</div>"; // col-lg-6
                        echo "</div>"; // row
                        echo "</div>\n"; // book-info
                        if ($cnt % 3 == 0 && $cnt < $total_books) {
                            echo $block_footer;
                            echo $block_header;
                        }
                    }
                    $boxes_on_last_page = $cnt % 3;
                    if ($boxes_on_last_page > 0) {
                        $spacers_needed = 3 - $boxes_on_last_page;
                        for ($i = 0; $i < $spacers_needed; $i++) {
                            echo "<div class='book-spacer'><p><br></p></div>\n";
                        }
                    }
                    echo $block_footer;
                    $record_cnt++;
                    if ($debug && $record_cnt > 3) {
                        break;
                    }
                }
            }
        } else {
            echo "<div class=\"starter-template\">\n";
            echo "<img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>\n";
            echo "</div>\n";
        }
        ?>
    </div>
    <?php require_once("include/footer.php"); ?>
</body>
</html>
