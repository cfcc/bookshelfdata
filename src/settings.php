<!DOCTYPE html>
<html lang="en">
<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 10/5/16
 * Time: 9:02 AM
 */
$title = "Bookshelf Data";
$page_title = "Settings";

session_start();
?>
<head>
    <?php require_once "include/header.php"; ?>
</head>
<body>
<?php require_once "include/navbar.php"; ?>

<div class="container">
    <div class="starter-template">
    <?php
    if (isset($_SESSION['user_info'])) {
        require_once('include/app_config.php');
        require_once('vendor/cfcc/messages-module/MessagesModuleClass.php');
        $obj = new MessagesModuleUtil(DEFAULT_INI_LOCATION);

        $obj->includeScripts();

        try {
            $all_messages = $obj->generateMessageEditor();
            echo $all_messages;
        } catch (Exception $e) {
            echo "Error" . $e->getMessage();
        }
    } else {
        echo "<img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>\n";
    }
    ?>
    </div>
</div>
<?php require_once "include/footer.php"; ?>
</body>
</html>
