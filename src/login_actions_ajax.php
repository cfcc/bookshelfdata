<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 8/22/16
 * Time: 4:43 PM
 */
require_once("vendor/cfcc/cfcc-ad-authentication-module/AuthenticationModuleClass.php");

session_start();

try {
    $authenticator = new AuthenticationModule('/home/bookshelfdata/authentication_module.ini');
    $authentication_result = $authenticator->authenticate($_POST['username'], $_POST['password']);
    if ($authentication_result) {
        $_SESSION['user_info'] = $authenticator->get_user_info();
        $_SESSION['messagesModuleSecurityLevel'] = 2;
        header('HTTP/1.1 200 Authentication: Success');
    } else {
        header('HTTP/1.1 401 Authentication: Failure');
        $_SESSION['messagesModuleSecurityLevel'] = 0;
    }
} catch(Exception $e) {
    header('HTTP/1.1 500 ' . "Caught Exception:" . $e->getMessage());
}
