<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/15/16
 * Time: 2:04 PM
 */
require_once("include/app_config.php");

$config = load_config();

if (empty($menu)) {
    $menu = array(
        'Home' => 'index.php',
        'Import' => 'import.php',
        'Report' => 'report.php',
        'Settings' => 'settings.php',
    );
    if (isset($DEBUG)) {
        $menu['Debug'] = array(
            'Download CSV File' => "download.php",
            'Test Book Grouping' => "test.php"
        );
    }
}
if (!empty($page_title)) {
    $active_page = $page_title;
} else {
    $active_page = 'Home';
}
?>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?php echo $config['general']['brand']; ?></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php
                    foreach ($menu as $label => $url) {
                        if (is_array($url)) {
                            echo "<li class='dropdown'>\n";
                            echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>$label <span class='caret'></span></a>\n";
                            echo "<ul class='dropdown-menu'>\n";
                            foreach ($url as $label2 => $url2) {
                                echo "<li><a href='$url2'>$label2</a></li>\n";
                            }
                            echo "</ul>\n";
                            echo "</li>\n";
                        } else {
                            if ($active_page == $label) {
                                print("                <li class='active'><a href='#'>" . $label . "</a></li>\n");
                            } else {
                                print("                <li><a href='" . $url . "'>" . $label . "</a></li>\n");
                            }
                        }
                    }
                    ?>
                </ul>
                <form class="navbar-form navbar-right">
                    <?php
                    if (isset($_SESSION['user_info'])) {
                        ?>
                        <div class="form-group">
                            <input type="button" class="btn btn-default" onclick="do_logout();" value="Logout">
                        </div>
                        <?php
                    } else {
                        ?>
                        <!-- the login form starts here -->
                        <div class="form-group">
                            <input type="text" class="form-control" id="username" placeholder="Username" autofocus>
                            <input type="password" class="form-control" id="password" placeholder="Password">
                        </div>
                        <input type="button" class="btn btn-default" onclick="authenticate();" value="Login">
                        <?php
                    }
                    ?>
                </form>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
