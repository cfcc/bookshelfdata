<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 8/4/16
 * Time: 4:17 PM
 */

function endswith($string, $test) {
    $strlen = strlen($string);
    $testlen = strlen($test);
    if ($testlen > $strlen) return false;
    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
}

function getTerm($record) {
    // TODO: is there a more efficient way to do this when we don't know the book array indexes?
    $campus = array(
        'W' => 'WILM',
        'N' => 'NORTH'
    );
    $term = null;
    foreach ($record['books'] as $book) {
        $term_id = $book['term_desc'];
        $year = substr($term_id, -2, 2);
        $campus_code = substr($term_id, -3, 1);
        $semester = substr($term_id, 0, strlen($term_id) - 3);
        $term = $campus[$campus_code] . " " . $semester . " 20" . $year;
        break;
    }
    return $term;
}