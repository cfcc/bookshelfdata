<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 6/9/16
 * Time: 10:22 AM
 */

include_once "include/app_config.php";

$config = load_config();

if (!empty($config)) {
    $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['dbname'] . ';charset=' . $config['db']['charset'];
} else {
    $dsn = null;
}

/*
 * section_number
 * section_instr_cu_id
 * section_note
 * college_depart_code
 * course_name
 * CFCC-Section
 * course_description
 * course_note
 * term_desc
 * required_code
 * in_isbn
 * in_title
 * in_author
 * used_isbn
 * in_edition
 * in_copyright
 * in_publisher
 * in_listprice_new
 * in_listprice_used
 * cu_lname
 * cu_fname
 * section_actual_enrollment
 * estimated_sales
 * section_estimated_enrollment
 * section_choice_min_qty
 * footer
 * section_id
 * term_id
 * course_id
 * deptcourse
 * no_text
 * in_serial_code
 * in_id
 * itm_new_only
 * used_in_id
 * used_serial_code
 * course_print_note
 * new_rental_fee
 * used_rental_fee
 */

const POS_SECTION_NUMBER = 0;
const POS_SECTION_INSTR_CU_ID = 1;
const POS_SECTION_NOTE = 2;
const POS_COLLEGE_DEPART_CODE = 3;
const POS_COURSE_NAME = 4;
const POS_COURSE_DESCRIPTION = 5;
const POS_COURSE_NOTE = 6;
const POS_TERM_DESC = 7;
const POS_REQUIRED_CODE = 8;
const POS_IN_ISBN = 9;
const POS_IN_TITLE = 10;
const POS_IN_AUTHOR = 11;
const POS_USED_ISBN = 12;
const POS_IN_EDITION = 13;
const POS_IN_COPYRIGHT = 14;
const POS_IN_PUBLISHER = 15;
const POS_IN_LISTPRICE_NEW = 16;
const POS_IN_LISTPRICE_USED = 17;
const POS_CU_LNAME = 18;
const POS_CU_FNAME = 19;
const POS_SECTION_ACTUAL_ENROLLMENT = 20;
const POS_ESTIMATED_SALES = 21;
const POS_SECTION_ESTIMATED_ENROLLMENT = 22;
const POS_SECTION_CHOICE_MIN_QTY = 23;
const POS_FOOTER = 24;
const POS_SECTION_ID = 25;
const POS_TERM_ID = 26;
const POS_COURSE_ID = 27;
const POS_DEPTCOURSE = 28;
const POS_NO_TEXT = 29;
const POS_IN_SERIAL_CODE = 30;
const POS_IN_ID = 31;
const POS_ITM_NEW_ONLY = 32;
const POS_USED_IN_ID = 33;
const POS_USED_SERIAL_CODE = 34;
const POS_COURSE_PRINT_NOTE = 35;
const POS_NEW_RENTAL_FEE = 36;
const POS_USED_RENTAL_FEE = 37;

/** loadColsFromFile
 * Load specific columns from the CSV file, convert if necessary, and import into the database.
 * @param $filename: the name of the local file
 */
function loadColsFromFile($filename, $dsn, $username, $password) {
    // FIXME: should we import the full file to be able to export a sample again?
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $username, $password, $opt);

    $stmt = $pdo->prepare('INSERT INTO books (
                               dept_code,
                               section_number,
                               course_number,
                               instructor_id,
                               isbn,
                               course_note,
                               course_desc,
                               section_note,
                               term_desc,
                               required_code,
                               title,
                               author,
                               use_isbn,
                               edition,
                               copyright,
                               publisher,
                               list_price_new,
                               list_price_used,
                               rental_fee_new,
                               rental_fee_used,
                               footer,
                               dept_course,
                               no_text,
                               serial_code,
                               item_new_only,
                               course_print_note,
                               instructor_last,
                               instructor_first
                            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
    if (!$stmt) {
        echo "\nPDO::errorInfo():\n";
        print_r($pdo->errorInfo());
    } else {
        // Before loading the new data, make sure the table is blank
        // TODO: this delete should probably be optional
        $pdo->query("DELETE FROM books");
        
        $handle = fopen($filename, 'r');
        if ($handle !== FALSE) {
            $row = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 0) {
                    $values = array(
                        $data[POS_COLLEGE_DEPART_CODE],
                        $data[POS_SECTION_NUMBER],
                        $data[POS_COURSE_NAME],
                        $data[POS_SECTION_INSTR_CU_ID],
                        (float) $data[POS_IN_ISBN],
                        $data[POS_COURSE_NOTE],
                        $data[POS_COURSE_DESCRIPTION],
                        $data[POS_SECTION_NOTE],
                        $data[POS_TERM_DESC],
                        $data[POS_REQUIRED_CODE],
                        $data[POS_IN_TITLE],
                        $data[POS_IN_AUTHOR],
                        (float) $data[POS_USED_ISBN],
                        $data[POS_IN_EDITION],
                        $data[POS_IN_COPYRIGHT],
                        $data[POS_IN_PUBLISHER],
                        (float) $data[POS_IN_LISTPRICE_NEW],
                        (float) $data[POS_IN_LISTPRICE_USED],
                        (float) $data[POS_NEW_RENTAL_FEE],
                        (float) $data[POS_USED_RENTAL_FEE],
                        $data[POS_FOOTER],
                        $data[POS_DEPTCOURSE],
                        $data[POS_NO_TEXT],
                        $data[POS_IN_SERIAL_CODE],
                        $data[POS_ITM_NEW_ONLY],
                        $data[POS_COURSE_PRINT_NOTE],
                        $data[POS_CU_LNAME],
                        $data[POS_CU_FNAME]
                    );
                    try {
                        $stmt->execute($values);
                        $result = $stmt->rowCount();
                        print("Inserting row $row: [$result]\n");
                    } catch(PDOException $ex) {
                        print("Error inserting row $row: " . $ex->getMessage());
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }
}

function getBooksByCourse2($dsn, $username, $password, $dept_filter="", $course_filter="") {
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $username, $password, $opt);
    if ($course_filter && $dept_filter) {
        $stmt = $pdo->prepare("SELECT * FROM books WHERE dept_code=? AND course_number=? ORDER BY dept_code, course_number, section_number");
        $args = array($dept_filter, $course_filter);
    } elseif ($dept_filter) {
        $stmt = $pdo->prepare("SELECT * FROM books WHERE dept_code=? ORDER BY dept_code, course_number, section_number");
        $args = array($dept_filter);
    } elseif ($course_filter) {
        $stmt = $pdo->prepare("SELECT * FROM books WHERE course_number=? ORDER BY dept_code, course_number, section_number");
        $args = array($course_filter);
    } else {
        $stmt = $pdo->prepare("SELECT * FROM books ORDER BY dept_code, course_number, section_number");
        $args = null;
    }
    $stmt->execute($args);

    $section_list = array();

    foreach($stmt->fetchAll() as $row) {
        $key = $row['dept_course'];
        $section_list[$key][$row['isbn']] = $row;
    }

    $cards = array();

    foreach($section_list as $key => $row) {
        $book_list = array_keys($row);
        sort($book_list);
        $book_group = implode("-", $book_list);
        $parts = explode("-", $key);
        $course = $parts[0] . "-" . $parts[1];
        $section = $parts[2];
        if (!array_key_exists($book_group, $cards)) {
            $cards[$book_group] = array(
                'books' => $row,
                'courses' => array($course => array($section)),
                'required' => 0,
                'choices' => 0
            );
        } else {
            if (array_key_exists($course, $cards[$book_group]['courses'])) {
                $cards[$book_group]['courses'][$course][] = $section;
            } else {
                $cards[$book_group]['courses'][$course] = array($section);
            }
        }
    }
    foreach ($cards as $key => $record) {
        foreach ($record['books'] as $book) {
            if ($book['required_code'] == 'R') {
                $cards[$key]['required']++;
            } elseif ($book['required_code'] == 'C') {
                $cards[$key]['choices']++;
            }
        }
        if ($cards[$key]['choices'] > 0) {
            // In this case add one more to the required count to indicate the student must choose at least 1 of the
            // books presented as a choice, in addition to any books that are required.
            $cards[$key]['required']++;
        }
    }
    return $cards;
}

// This function goes through the card list to see if any of them are use exclusively for the sections of a Course, or even
// all Sections and all Courses for a Department.
function findExclusive($cards) {
    $books_by_dept = array();
    $books_by_course = array();
    foreach ($cards as $book_group => $card_info) {
        foreach ($card_info['courses'] as $course_id => $sections) {
            $dept = explode("-", $course_id)[0];
            if (array_key_exists($dept, $books_by_dept)) {
                if (array_key_exists($book_group, $books_by_dept[$dept])) {
                    $books_by_dept[$dept][$book_group] += 1;
                } else {
                    $books_by_dept[$dept][$book_group] = 1;
                }
            } else {
                $books_by_dept[$dept] = array($book_group => 1);
            }

            if (array_key_exists($course_id, $books_by_course)) {
                if (array_key_exists($book_group, $books_by_course[$course_id])) {
                    $books_by_course[$course_id][$book_group] += 1;
                } else {
                    $books_by_course[$course_id][$book_group] = 1;
                }
            } else {
                $books_by_course[$course_id] = array($book_group => 1);
            }
        }
    }
    /*
    $exclusive_books = array(
        'dept' => $books_by_dept,
        'course' => $books_by_course
    );
    */

    $exclusive_books = array(
        'dept' => array(),
        'course' => array()
    );
    foreach ($books_by_dept as $dept => $book_group_list) {
        if (count(array_keys($book_group_list)) == 1) {
            $k = array_keys($book_group_list)[0];
            if (array_key_exists($k, $exclusive_books['dept'])) {
                $exclusive_books['dept'][$k][] = $dept;
            } else {
                $exclusive_books['dept'][$k] = array($dept);
            }
        }
    }
    foreach ($books_by_course as $course => $book_group_list) {
        if (count(array_keys($book_group_list)) == 1) {
            $k = array_keys($book_group_list)[0];
            if (array_key_exists($k, $exclusive_books['course'])) {
                $exclusive_books['course'][$k][] = $course;
            } else {
                $exclusive_books['course'][$k] = array($course);
            }
        }
    }

    return $exclusive_books;
}

function dumpBooks($dsn, $username, $password) {
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $username, $password, $opt);

    $stmt = $pdo->query("SELECT * FROM books");
    return $stmt->fetchAll();
}