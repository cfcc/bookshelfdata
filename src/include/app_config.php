<?php

const DEFAULT_INI_LOCATION = '/home/bookshelfdata/settings.ini';

function load_config(string $config_filename=NULL): array {
    if ($config_filename == NULL) {
        $config_filename = DEFAULT_INI_LOCATION;
    }
    return parse_ini_file($config_filename, true);
}
