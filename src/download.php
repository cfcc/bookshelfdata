<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 7/9/16
 * Time: 8:54 AM
 */

// First see if the sample data file exists, if so just send it to the client.  If not, then send a CSV version of the
// records in the database.

session_start();

if (isset($_SESSION['user_info'])) {

    $file = 'Bookshelf.csv';

    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
    } else {
        require_once("include/app_config.php");
        require_once("include/database.php");

        $config = load_config();

        $dsn = 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['dbname'] . ';charset=' . $config['db']['charset'];

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        $records = dumpBooks($dsn, $config['db']['username'], $config['db']['password']);

        foreach ($records as $row) {
            fputcsv($output, $row);
        }

    }
    exit;
} else {
    ?>
    <html lang="en">
    <head>
        <?php require_once "include/header.php"; ?>
    </head>
    <body>
    <?php require_once "include/navbar.php"; ?>
    <div class="container">
        <div class='starter-template'>
            <img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>
        </div>
        <?php require_once "include/footer.php"; ?>
    </div>
    </body>
    </html>
    <?php
}
?>

