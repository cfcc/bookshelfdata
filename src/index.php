<!DOCTYPE html>
<html lang="en">
<?php
$title = "Bookshelf Data";
$page_title = "Home";
session_start();
?>
<head>
    <?php require_once "include/header.php"; ?>
</head>
<body>
<?php require_once "include/navbar.php"; ?>

<div class="container">

    <?php
    if (isset($_SESSION['user_info'])) {
        ?>
        <div class="starter-template-alt">
            <div class="row">
                <div class="col-lg-4">
                    <h2>Step 1</h2>
                </div>
                <div class="col-lg-4">
                    <h2>Step 2</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a class="btn btn-default" style="width: 250px;" href="import.php">Import CSV File</a>
                </div>
                <div class="col-lg-4">
                    <a class="btn btn-default" style="width: 250px;" href="report.php">Report from Database</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3>Note:</h3>
                    <p>For best results "print" to a PDF file first and then use Adobe Reader to send the cards to the
                        printer.</p>
                </div>
            </div>
        </div>
        <?php
    } else {
        echo "<div class='starter-template'>\n";
        echo "<img src='resources/images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>\n";
        echo "</div>\n";
    }
    ?>
</div>
<?php require_once "include/footer.php"; ?>
</body>
</html>
