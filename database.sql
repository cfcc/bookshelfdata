-- MySQL dump 10.16  Distrib 10.1.48-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bookshelf
-- ------------------------------------------------------
-- Server version	10.1.48-MariaDB-0+deb9u2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- CREATE DATABASE bookshelf;

-- USE bookshelf;

--
-- Table structure for table `MessagesModule_Messages`
--

DROP TABLE IF EXISTS `MessagesModule_Messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MessagesModule_Messages` (
                                           `id` varchar(50) DEFAULT NULL,
                                           `display_title` varchar(50) DEFAULT NULL,
                                           `message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `dept_code` varchar(10) DEFAULT NULL,
                         `section_number` varchar(10) DEFAULT NULL,
                         `instructor_id` varchar(7) DEFAULT NULL,
                         `isbn` varchar(25) DEFAULT NULL,
                         `course_note` text,
                         `course_desc` text,
                         `section_note` text,
                         `term_desc` varchar(25) DEFAULT NULL,
                         `required_code` char(1) DEFAULT 'N',
                         `title` varchar(200) DEFAULT NULL,
                         `author` varchar(50) DEFAULT NULL,
                         `use_isbn` varchar(25) DEFAULT NULL,
                         `edition` varchar(10) DEFAULT NULL,
                         `copyright` varchar(20) DEFAULT NULL,
                         `publisher` varchar(50) DEFAULT NULL,
                         `list_price_new` float DEFAULT '0',
                         `list_price_used` float DEFAULT '0',
                         `footer` text,
                         `dept_course` varchar(25) DEFAULT NULL,
                         `no_text` char(1) DEFAULT '',
                         `serial_code` varchar(25) DEFAULT NULL,
                         `item_new_only` char(1) DEFAULT 'N',
                         `course_print_note` char(1) DEFAULT 'N',
                         `course_number` varchar(25) DEFAULT NULL,
                         `instructor_first` varchar(25) DEFAULT NULL,
                         `instructor_last` varchar(25) DEFAULT NULL,
                         `rental_fee_new` float DEFAULT '0',
                         `rental_fee_used` float DEFAULT '0',
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1847019 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-03 14:38:20
