app_name = bookshelfdata
win_home = /c/Users/jfriant80/src/$(app_name)
export SERVICE_NAME=$(app_name)
build:
	@build-image.sh
run:
	docker run --detach -p 8080:80 -v $(win_home)/instance:/home/$(app_name)/:ro $(app_name)
kill:
	@echo 'Killing container...'
	@docker ps | grep $(app_name) | awk '{print $$1}' | xargs docker stop
rebuild: kill build run
tag:
	@docker tag $(app_name) nexus.ad.cfcc.edu:18095/$(app_name)
push: build tag
	@docker push nexus.ad.cfcc.edu:18095/$(app_name)
install-composer:
	@install-composer.sh
