FROM php:8-apache-buster

RUN \
apt-get update \
    && apt-get install -y --no-install-recommends \
    libldap2-dev \
    unixodbc-dev \
    gnupg \
    libssh2-1 \
    libssh2-1-dev \
    libicu-dev \
    && apt-get update \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap \
    && docker-php-ext-install pdo pdo_mysql mysqli \
    && docker-php-ext-enable mysqli \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY start-apache.sh /usr/local/bin

# Copy application source
COPY src /var/www/html
COPY resources /var/www/html/resources
# Copy required packages supplied through Composer
COPY vendor /var/www/html/vendor

RUN chown -R www-data:www-data /var/www \
    && chmod 755 /usr/local/bin/start-apache.sh

CMD ["start-apache.sh"]
