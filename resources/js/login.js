/**
 * Created by jfriant80 on 8/22/16.
 */
function authenticate(next_url) {
    let username = $('#username').val();
    let password = $('#password').val();
    if (username === "") {
        alert("Username cannot be empty");
        $('#username').focus();
        return;
    }
    if (password === "") {
        alert("Password cannot be empty");
        $('#password').focus();
        return;
    }
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: 'login_actions_ajax.php',
        data:{
            username:username,
            password:password
        },
        success:function(data,textStatus){
            if (is_valid_domain(next_url)) {
                window.location=next_url;
            } else {
                window.location.reload();
                /* console.log('Logged in successfully'); */
            }
        },
        error: function (xhr,textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}

function is_valid_domain(url) {
    let patterns = {};
    let success = false;

    patterns.protocol = '^^(http(s)?(:\/\/))?(www\.)?';
    patterns.domain = '([a-zA-Z0-9-_\.]+)';

    let url_regex = new RegExp(patterns.protocol + patterns.domain, 'gi');
    let match = url_regex.exec(url);
    if (match) {
        console.log('[DEBUG] ' + match[5] + " == " + window.location.hostname);
        if (match[5] === window.location.hostname){
            success = true;
        }
    }
    return success;
}

function do_logout() {
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: 'logout_actions_ajax.php',
        data:{},
        success:function(data,textStatus){
            window.location.reload();
        },
        error: function (xhr,textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}
