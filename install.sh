#!/bin/bash
docker-compose up -d bookshelf-db
sleep 5
docker cp database.sql bookshelfdata_db:/database.sql
read -s -p "MariaDB Password:" MARIADB_PASSWORD
echo ""
docker exec -i bookshelfdata_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD' < /database.sql"
read -s -p "Application Password:" APP_PASSWORD
echo ""

# FIXME: this is failing
echo "CREATE USER 'bookstore'@'%' IDENTIFIED BY '$APP_PASSWORD'; GRANT ALL PRIVILEGES ON \`bookshelf\`.\* TO 'bookstore'@'%';" |
docker exec -i bookshelfdata_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD'"
