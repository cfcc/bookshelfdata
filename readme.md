BookshelfData
=============

This is a PHP-based program used to import a CSV data file from a Booklog
system and turn it into a shelf card that display the course, book, etc.

Although you can print the shelf cards directly from a browser, it often
works best to "print" to a PDF file and then use Adobe Reader to print,
especially for alternate paper sizes.

Database Configuration
----------------------

A sample database configuration file has been provided.  When testing or
in production create a copy called config.php and enter the actual
information there.  Do not commit passwords to the repository.

Additional Libraries
--------------------

This project uses two other CFCC-developed libraries:

 1. cfcc-ad-authentication
 2. cfcc-messages-module

Docker
------

The latest version now runs in Docker and uses composer to pull the required libraries from the BitBucket git
repository.

When testing with docker-compose you can create a `.env` file in the source directory with the path to the instance
configuration and database files.  When using Docker Desktop in a Windows environment you'll need to create the
directory on a NTFS partition, or it won't be mounted properly.  For example:

    DOCKERDIR=/c/Users/USERNAME_HERE/docker

To build from source:

    docker build -t bookshelfdata:latest .


Configuration Files
-------------------

With the Docker image, configuration files are no longer kept with the PHP files.  You now create two INI files, one
for the database settings and one for the authentication module.

Sample database configuration (settings.ini)

    [db]
    host = SERVER_NAME
    dbname = bookshelf
    username = DB_USERNAME_HERE
    password = DB_PASSWORD_HERE
    charset = utf8

    [general]
    brand = Bookshelf Cards

Sample authentication module configuration (authentication_module.ini)

    authenticationModule_domain_controller = DOMAIN_CONTROLLER_IP
    authenticationModule_base_dn = "dc=ad,dc=example,dc=com"
    authenticationModule_account_suffix = @ad.example.com

The authentication module is used for simple LDAP authentication against an Active Directory domain controller.

Set up the Database
-------------------

Start the database container

    docker-compose up -d bookshelf-db

Copy the database structure file into the container for later.

    docker cp database.sql bookshelfdata_db:/database.sql

Start a shell in the container.

    docker exec -it bookshelfdata_db /bin/bash

Start a database session.

    mariadb -uroot -p

Create the database and add a user account

    CREATE DATABASE bookshelf;
    CREATE USER 'bookstore'@'%' IDENTIFIED BY 'PASSWORD_GOES_HERE';
    GRANT ALL PRIVILEGES ON `bookshelf`.* TO 'bookstore'@'%';

The back in the shell import the database file to create the basic structure

    mariadb -uroot -p bookshelf < ./database.sql

Exit out of the container.
